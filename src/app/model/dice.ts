import {DiceType} from "./dice-type";
export class Dice {
  id: number;
  type: DiceType;
  count: number;

}
