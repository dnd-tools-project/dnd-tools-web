import {StatType} from "./stat-type";
export class Stat {
  id: number;
  type: StatType;
  value: number;
  bonus: number;
}
