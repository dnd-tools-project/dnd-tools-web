import {StatType} from "./stat-type";
export class SavingThrow {
  id: number;
  type: StatType;
  hasProficiency: boolean;
}
