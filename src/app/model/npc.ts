import {Weapon} from "./weapon";
import {SavingThrow} from "./saving-throw";
import {Spell} from "./spell";
import {Stat} from "./stat";
import {Skill} from "./skill";
export class Npc {
  id: number;
  name: string;
  maxHp: string;
  currentHp: string;
  armorClass: number;
  cls: string;
  race: string;
  skills: Array<Skill>;
  stats: Array<Stat>;
  spells: Array<Spell>;
  savingThrows: Array<SavingThrow>;
  proficiencyBonus: number;
  items: Array<any>;
  notes: string;
  currentWeapon: Weapon;
  proficiencies: string;
}
