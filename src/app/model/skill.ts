import {StatType} from "./stat-type";
import {SkillType} from "./skill-type";
export class Skill {
  id: number;
  statType: StatType;
  skillType: SkillType;
  hasProficiency: boolean;
}
