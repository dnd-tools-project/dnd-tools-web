import {Proficiency} from "./proficiency";
export class Trait {
  id: number;

  name: string;

  description: string;

  proficiencies: Array<Proficiency>;
}
