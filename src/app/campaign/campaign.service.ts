import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Campaign} from "../model/campaign";
import {HttpClient} from "@angular/common/http";
@Injectable({
  providedIn: 'root',
})
export class CampaignService {

  constructor(private http: HttpClient) {
  }

  public getCampaign(id: number): Observable<Campaign> {
    let url = '/api/campaigns/' + id;

    return this.http.get<Campaign>(url);
  }

}
