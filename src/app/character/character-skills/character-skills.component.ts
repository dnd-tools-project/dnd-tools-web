import {Component, Input, Output, EventEmitter, OnInit} from "@angular/core";
import {Character} from "../../model/character";
import {StatType} from "../../model/stat-type";
import {SkillType} from "../../model/skill-type";
import {Skill} from "../../model/skill";
import {MatCheckboxChange} from "@angular/material/checkbox";
import {CharacterService} from "../character.service";
/**
 * Created by vesko on 24.5.2021 г..
 */
@Component({
  selector: 'app-character-skills',
  templateUrl: './character-skills.component.html',
  styleUrls: ['./character-skills.component.scss']
})
export class CharacterSkillsComponent implements OnInit {

  @Input()
  character: Character;

  skills: Array<Array<Skill>> = [];

  constructor(private characterService: CharacterService) {

  }

  ngOnInit(): void {
    this.characterChange();
  }

  characterChange() {
    this.skills = [];
    let str: Array<Skill> = [];
    let dex: Array<Skill> = [];
    let int: Array<Skill> = [];
    let wis: Array<Skill> = [];
    let cha: Array<Skill> = [];
    for (let skill of this.character.skills) {
      if (skill.statType === StatType.Strength) {
        str.push(skill);
      } else if (skill.statType === StatType.Dexterity) {
        dex.push(skill);
      } else if (skill.statType === StatType.Intelligence) {
        int.push(skill);
      } else if (skill.statType === StatType.Wisdom) {
        wis.push(skill);
      } else if (skill.statType === StatType.Charisma) {
        cha.push(skill);
      }
    }
    this.skills.push(str);
    this.skills.push(dex);
    this.skills.push(int);
    this.skills.push(wis);
    this.skills.push(cha);
  }

  getStatBonus(statType: StatType): number {
    for (let stat of this.character.stats) {
      if (stat.type === statType) {
        return stat.bonus;
      }
    }
    return 0;
  }

  getShortStat(statType: StatType): string {
    return StatType[statType].substring(0, 3);
  }

  getStatModifier(statType: StatType): number {
    for (let stat of this.character.stats) {
      if (stat.type === statType) {
        return stat.bonus;
      }
    }
    return 0;
  }

  onChange(event: MatCheckboxChange, skill: Skill) {
    this.characterService.updateSkill(skill).subscribe(res => {
      skill = res;
    })
  }

}
