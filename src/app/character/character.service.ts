import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Character} from "../model/character";
import {Spell} from "../model/spell";
import {Item} from "../model/item";
import {SpellSlot} from "../model/spell-slot";
import {Counter} from "../model/counter";
import {Skill} from "../model/skill";
import {SavingThrow} from "../model/saving-throw";
import {Weapon} from "../model/weapon";
import {Stat} from "../model/stat";
import {Trait} from "../model/trait";
import {Proficiency} from "../model/proficiency";
@Injectable({
  providedIn: 'root',
})
export class CharacterService {

  constructor(private http: HttpClient) {

  }

  public getCharacter(name: string): Observable<Character> {
    let url = '/api/characters/' + name;

    return this.http.get<Character>(url);
  }

  public saveCharacter(character: Character): Observable<Character> {
    let url = '/api/characters';

    return this.http.put<Character>(url, character);
  }

  public deleteItem(item, character: Character): Observable<any> {
    let url = '/api/characters/' + character.id + '/items/' + item.id;

    return this.http.delete(url);
  }

  public deleteSpell(spell: Spell, character: Character): Observable<any> {
    let url = '/api/characters/' + character.id + '/spells/' + spell.id;

    return this.http.delete(url);
  }

  public createSlot(spellSlot: SpellSlot, charId: number): Observable<SpellSlot> {
    let url = '/api/characters/' + charId + '/slots';

    return this.http.post<SpellSlot>(url, spellSlot);
  }

  public createCounter(counter: Counter, charId: number): Observable<Counter> {
    let url = '/api/characters/' + charId + '/counters';

    return this.http.post<Counter>(url, counter);
  }


  public updateSlot(spellSlot: SpellSlot): Observable<SpellSlot> {
    let url = '/api/slots';

    return this.http.put<SpellSlot>(url, spellSlot);
  }

  public updateCounter(counter: Counter): Observable<Counter> {
    let url = '/api/counters';

    return this.http.put<Counter>(url, counter);
  }

  public deleteSlot(slotId, character: Character): Observable<any> {
    let url = '/api/characters/' + character.id + '/slots/' + slotId;

    return this.http.delete(url);
  }

  public deleteCounter(counterId, character: Character): Observable<any> {
    let url = '/api/characters/' + character.id + '/counters/' + counterId;

    return this.http.delete(url);
  }

  public getAllSpells(): Observable<Array<Spell>> {
    let url = '/api/spells';

    return this.http.get<Array<Spell>>(url);
  }

  public getSpellsForClassAndLevel(cls: string, level: number): Observable<Array<Spell>> {
    let url = '/api/spells/' + cls + '/' + level;

    return this.http.get<Array<Spell>>(url);
  }

  public addItem(charId: number, item: Item): Observable<any> {
    let url = '/api/characters/' + charId + '/items';

    return this.http.post(url, item);
  }

  public updateItem(item: Item): Observable<any> {
    let url = '/api/items';

    return this.http.put(url, item);
  }

  public addSpell(charId: number, spell: Spell): Observable<Spell> {
    let url = '/api/characters/' + charId + '/spells';

    return this.http.put<Spell>(url, spell);
  }

  public updateSkill(skill: Skill): Observable<Skill> {
    let url = "/api/skills";

    return this.http.put<Skill>(url, skill);
  }

  public updateThrow(saveTh: SavingThrow): Observable<SavingThrow> {
    let url = "/api/throws";

    return this.http.put<SavingThrow>(url, saveTh);
  }

  public setWeapon(weapon: Weapon, charId: number): Observable<Weapon> {
    let url = '/api/characters/' + charId + '/weapon';

    return this.http.put<Weapon>(url, weapon);
  }

  public updateStat(stat: Stat): Observable<Stat> {
    let url = '/api/stats';

    return this.http.put<Stat>(url, stat);
  }

  public findTraitsByName(name: string): Observable<Array<Trait>> {
    let url = '/api/traits/' + name;

    return this.http.get<Array<Trait>>(url);
  }

  public addTrait(charId: number, trait: Trait): Observable<Trait> {
    let url = '/api/characters/' + charId + '/traits';

    return this.http.put<Trait>(url, trait);
  }

  public deleteTrait(trait: Trait, character: Character): Observable<any> {
    let url = '/api/characters/' + character.id + '/traits/' + trait.id;

    return this.http.delete(url);
  }

  public findProfsByName(name: string): Observable<Array<Proficiency>> {
    let url = '/api/proficiencies/' + name;

    return this.http.get<Array<Proficiency>>(url);
  }

  public addProf(charId: number, prof: Proficiency): Observable<Proficiency> {
    let url = '/api/characters/' + charId + '/profs';

    return this.http.put<Proficiency>(url, prof);
  }

  public deleteProf(prof: Proficiency, character: Character): Observable<any> {
    let url = '/api/characters/' + character.id + '/profs/' + prof.id;

    return this.http.delete(url);
  }
}
