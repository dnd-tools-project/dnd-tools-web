import {Component, Input, Output, EventEmitter, ViewChild} from "@angular/core";
import {Character} from "../../model/character";
import {Spell} from "../../model/Spell";
import {animate, style, transition, state, trigger} from "@angular/animations";
import {MatDialog} from "@angular/material/dialog";
import {SpellSlot} from "../../model/spell-slot";
import {Item} from "../../model/item";
import {CharacterService} from "../character.service";
import {Counter} from "../../model/counter";
import {MatTable} from "@angular/material/table";
/**
 * Created by vesko on 23.5.2021 г..
 */

@Component({
  selector: 'app-character-spells',
  templateUrl: './character-spells.component.html',
  styleUrls: ['./character-spells.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class CharacterSpellsComponent {
  @Input()
  character: Character;

  expandedSpell: Spell | null;
  columns = ['name', 'spellSlot', 'delete'];

  slotDialogRef;
  spellDialogRef;
  counterDialogRef;

  newSpellSlot: SpellSlot;

  newCounter: Counter;

  spellToEdit: Spell | null;

  maxLevel: number;
  cls: string;

  spellsToSelect: Array<Spell>;

  @ViewChild(MatTable) table: MatTable<any>;

  constructor(private dialog: MatDialog, private characterService: CharacterService) {
  }

  openSlotDialog(slotDialog) {
    this.newSpellSlot = new SpellSlot();
    this.newSpellSlot.lvl = 1;
    this.slotDialogRef = this.dialog.open(slotDialog);
  }

  saveSlot() {
    this.newSpellSlot.available = this.newSpellSlot.max;
    this.characterService.createSlot(this.newSpellSlot, this.character.id).subscribe(res => {
      this.character.spellSlots.push(res);
    });
    this.closeDialog();
  }

  openCounterDialog(counterDialog) {
    this.newCounter = new Counter();
    this.counterDialogRef = this.dialog.open(counterDialog);
  }

  saveCounter() {
    this.newCounter.current = this.newCounter.max;
    this.characterService.createCounter(this.newCounter, this.character.id).subscribe(res => {
      this.character.counters.push(res);
    });
    this.closeDialog();
  }

  canSave(): boolean {
    return true;
  }

  getHighestSlot(): Array<number> {
    let slotLvls = [];
    let highestSlot = 0;
    if (this.character.spellSlots != null && typeof this.character.spellSlots != 'undefined' && this.character.spellSlots.length != 0) {
      for (let slot of this.character.spellSlots) {
        if (slot.lvl > highestSlot) {
          highestSlot = slot.lvl;
        }
      }
    } else {
      highestSlot = 0;
    }
    highestSlot += 1;
    for (let i = highestSlot; i <= 9; i++) {
      slotLvls.push(i);
    }
    return slotLvls;
  }

  closeDialog() {
    if (typeof this.slotDialogRef != 'undefined') {
      this.slotDialogRef.close();
    }
    if (typeof this.spellDialogRef != 'undefined') {
      this.spellDialogRef.close();
    }
    if (typeof this.counterDialogRef != 'undefined') {
      this.counterDialogRef.close();
    }
    this.spellsToSelect = [];
    this.maxLevel = 0;
    this.cls = '';
  }

  getDialogTitle(): string {
    return typeof this.spellToEdit.id != 'undefined' ? 'Edit Spell' : 'New Spell'
  }

  editSpell(spell, spellDialog) {
    this.cls = this.character.charClass;
    this.spellDialogRef = this.dialog.open(spellDialog);
  }

  deleteSpell(spell) {
    this.characterService.deleteSpell(spell, this.character).subscribe(res => {
      this.character.spells.splice(this.character.spells.indexOf(spell), 1);
      this.table.renderRows();
    });
  }

  onSlotDelete(event) {
    this.characterService.deleteSlot(event.id, this.character).subscribe(res => {
      this.character.spellSlots.splice(this.character.spellSlots.indexOf(event), 1);
    });
  }

  onCounterDelete(event) {
    this.characterService.deleteCounter(event.id, this.character).subscribe(res => {
      this.character.counters.splice(this.character.counters.indexOf(event), 1);
    });
  }

  searchSpells() {
    this.characterService.getSpellsForClassAndLevel(this.cls, this.maxLevel).subscribe((res: Array<Spell>) => {
      this.spellsToSelect = res;
    })
  }

  canSearch() {
    return typeof this.maxLevel != 'undefined' && typeof this.cls != 'undefined' && this.cls != '';
  }

  getAll() {
    this.characterService.getAllSpells().subscribe((res: Array<Spell>) => {
      this.spellsToSelect = res;
    })
  }

  addSpell(spell) {
    this.characterService.addSpell(this.character.id, spell).subscribe(res => {
      this.character.spells.push(res);
      this.table.renderRows();
    });
  }

  canAdd(spell) {
    if (typeof this.character.spells != 'undefined' && this.character.spells.length != 0) {
      for (let s of this.character.spells) {
        if (s.id === spell.id) {
          return false;
        }
      }
    }
    return true;
  }

}
