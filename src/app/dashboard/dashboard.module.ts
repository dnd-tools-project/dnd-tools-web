import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {RouterModule} from "@angular/router";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";
import {FlexLayoutModule} from "@angular/flex-layout";
import {DashboardComponent} from "./dashboard.component";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

const routes = [
  {
    path: 'dash',
    component: DashboardComponent,
  },
];

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forChild(routes),
    FormsModule,
    HttpClientModule,

    MatTooltipModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    FlexLayoutModule
  ],
  providers: []
})
export class DashboardModule {
}
