import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  playerName: string;

  constructor(public router: Router) {
  }

  ngOnInit(): void {
  }

  onEnter() {
    if (this.playerName === 'DM') {
      this.router.navigate(['dm']);
    } else {
      this.router.navigate(['player', this.playerName]);
    }
  }

}
