import {Component, OnInit} from "@angular/core";
import {Character} from "../model/character";
import {PlayerService} from "./player.service";
import {ActivatedRoute} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {Alignment} from "../model/alignment";
import {DiceType} from "../model/dice-type";
import {Dice} from "../model/dice";
import {Spell} from "../model/spell";
import {Proficiency} from "../model/proficiency";
import {Trait} from "../model/trait";
/**
 * Created by vesko on 24.5.2021 г..
 */

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  characters: Array<Character>;

  columns = ['name', 'delete'];

  characterDialogRef;
  confirmDialogRef;

  newCharacter: Character;
  playerName;

  characterToDelete: Character;

  alignments: Array<string> = [];

  dice: Array<string> = [];

  selectedDice: string;

  selectedAlignment: string;

  constructor(public playerService: PlayerService, public route: ActivatedRoute, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.playerName = params['id'];
      this.playerService.getCharactersByPlayer(params['id']).subscribe((res: Array<Character>) => {
        res.sort((n1: Character, n2: Character) => {
          return n1.name.localeCompare(n2.name);
        });

        this.characters = res;
      });
    });
  }

  onConfirm() {
    this.playerService.deleteCharacter(this.characterToDelete).subscribe(res => {
      this.playerService.getCharactersByPlayer(this.playerName).subscribe((res: Array<Character>) => {
        this.characters = res;
        this.confirmDialogRef.close();
      });
    })
  }

  onSave() {
    this.newCharacter.playerName = this.playerName;
    this.newCharacter.alignment = Alignment[this.selectedAlignment];
    this.newCharacter.hitDice.type = DiceType[this.selectedDice];
    this.newCharacter.hitDice.count = 1;
    this.playerService.crateCharacter(this.newCharacter).subscribe((res: Character) => {
      this.alignments = [];
      this.selectedAlignment = '';
      this.dice = [];
      this.selectedDice = '';
      this.playerService.getCharactersByPlayer(this.playerName).subscribe((res: Array<Character>) => {
        this.characters = res;
      });
    });

    this.characterDialogRef.close();
  }

  openConfirmDialog(confirmDialog, character: Character) {
    this.characterToDelete = character;
    this.confirmDialogRef = this.dialog.open(confirmDialog);
  }

  openCharacterDialog(characterDialog) {
    this.playerService.getNewCharacter().subscribe((res: Character) => {
      res.hitDice = new Dice();
      this.newCharacter = res;
      for (let alignment in Alignment) {
        if (typeof Alignment[alignment] != 'number') {
          this.alignments.push(Alignment[alignment]);
        }
      }

      for (let dice in DiceType) {
        if (typeof DiceType[dice] != 'number') {
          this.dice.push(DiceType[dice]);
        }
      }
      this.characterDialogRef = this.dialog.open(characterDialog);
    });
  }

  closeDialog() {
    if (typeof this.characterDialogRef != 'undefined') {
      this.alignments = [];
      this.selectedAlignment = '';
      this.dice = [];
      this.selectedDice = '';
      this.characterDialogRef.close();
    }
    if (typeof this.confirmDialogRef != 'undefined') {
      this.confirmDialogRef.close();
    }
  }

  canSave(): boolean {
    return this.newCharacter.name != null && typeof this.newCharacter.name != 'undefined' && this.newCharacter.name != '' &&
      this.newCharacter.charClass != null && typeof this.newCharacter.charClass != 'undefined' && this.newCharacter.charClass != '' &&
      this.newCharacter.race != null && typeof this.newCharacter.race != 'undefined' && this.newCharacter.race != '' &&
      this.selectedAlignment != null && typeof this.selectedAlignment != 'undefined' && this.selectedAlignment != '' &&
      this.selectedDice != null && typeof this.selectedDice != 'undefined' && this.selectedDice != '';
  }
}
