import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Character} from "../model/character";
import {Spell} from "../model/spell";
import {Proficiency} from "../model/proficiency";
import {Trait} from "../model/trait";
@Injectable({
  providedIn: 'root',
})
export class PlayerService {

  constructor(private http: HttpClient) {

  }

  getCharactersByPlayer(name: string): Observable<Array<Character>> {
    let url = '/api/players/' + name

    return this.http.get<Array<Character>>(url);
  }

  getNewCharacter(): Observable<Character> {
    let url = '/api/characters/new';

    return this.http.get<Character>(url);
  }

  crateCharacter(character: Character): Observable<Character> {
    let url = '/api/characters';

    return this.http.post<Character>(url, character);
  }

  deleteCharacter(character: Character): Observable<any> {
    let url = '/api/characters/delete';

    return this.http.post(url, character);
  }

  getSpells(): Observable<any> {
    let url = 'https://www.dnd5eapi.co/api/spells';

    return this.http.get(url);
  }

  getSpell(url): Observable<any> {
    return this.http.get('https://www.dnd5eapi.co' + url);
  }

  getTraits(): Observable<any> {
    let url = 'https://www.dnd5eapi.co/api/traits';

    return this.http.get(url);
  }

  getTrait(url): Observable<any> {
    return this.http.get('https://www.dnd5eapi.co' + url);
  }

  getProfs(): Observable<any> {
    let url = '/api/proficiencies';

    return this.http.get(url);
  }

  getProf(url): Observable<any> {
    return this.http.get('https://www.dnd5eapi.co' + url);
  }

  getProfByName(name): Observable<Proficiency> {
    return this.http.get<Proficiency>('/api/proficiencies/' + name);
  }

  addSpell(spell: Spell): Observable<Spell> {
    let url = '/api/spells';

    return this.http.put<Spell>(url, spell);
  }

  addProf(prof: Proficiency): Observable<Proficiency> {
    let url = '/api/proficiencies';

    return this.http.post<Proficiency>(url, prof);
  }

  addTrait(trait: Trait): Observable<Trait> {
    let url = '/api/traits';

    return this.http.post<Trait>(url, trait);
  }
}
